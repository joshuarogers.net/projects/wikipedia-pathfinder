import { Token, type TextEmphasisToken } from "../../src/tokens/tokenTypes";
import { tokenize } from "../../src/tokens/wikitextTokenizer";

describe("Emphasis", () => {
    test("can be italic", () => {
        const tokens = tokenize("''Hello World''");
        const expected: TextEmphasisToken = {
            type: 'text-emphasis',
            tokens: [
                {
                    type: 'text',
                    value: 'Hello World'
                }
            ],
            emphasisType: 'italic'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can be bold", () => {
        const tokens = tokenize("'''Hello World'''");
        const expected: TextEmphasisToken = {
            type: 'text-emphasis',
            tokens: [
                {
                    type: 'text',
                    value: 'Hello World'
                }
            ],
            emphasisType: 'bold'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can be bold and italic", () => {
        const tokens = tokenize("'''''Hello World'''''");
        const expected: TextEmphasisToken = {
            type: 'text-emphasis',
            tokens: [
                {
                    type: 'text',
                    value: 'Hello World'
                }
            ],
            emphasisType: 'bold/italic'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can start bold and italic and end italic only", () => {
        const tokens = tokenize("'''''Hello''' World''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello'
                    }
                ],
                emphasisType: 'bold/italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: ' World'
                    }
                ],
                emphasisType: 'italic'
            }
        ];

        expect(tokens).toEqual(expected);
    });

    test("can start bold and italic and end bold only", () => {
        const tokens = tokenize("'''''Hello'' World'''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello'
                    }
                ],
                emphasisType: 'bold/italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: ' World'
                    }
                ],
                emphasisType: 'bold'
            }
        ];

        expect(tokens).toEqual(expected);
    });

    test("can start bold and end bold and italic", () => {
        const tokens = tokenize("'''Hello ''World'''''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello '
                    }
                ],
                emphasisType: 'bold'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'World'
                    }
                ],
                emphasisType: 'bold/italic'
            }
        ];

        expect(tokens).toEqual(expected);
    });

    test("can start italic and end bold and italic", () => {
        const tokens = tokenize("''Hello '''World'''''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello '
                    }
                ],
                emphasisType: 'italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'World'
                    }
                ],
                emphasisType: 'bold/italic'
            }
        ];

        expect(tokens).toEqual(expected);
    });

    test("can nest bold inside italic", () => {
        const tokens = tokenize("''Hello '''World''' !''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello '
                    }
                ],
                emphasisType: 'italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'World'
                    }
                ],
                emphasisType: 'bold/italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: ' !'
                    }
                ],
                emphasisType: 'italic'
            }
        ];

        expect(tokens).toEqual(expected);
    });

    test("can nest italic inside bold", () => {
        const tokens = tokenize("'''Hello ''World'' !'''");
        const expected: Token[] = [
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'Hello '
                    }
                ],
                emphasisType: 'bold'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: 'World'
                    }
                ],
                emphasisType: 'bold/italic'
            },
            {
                type: 'text-emphasis',
                tokens: [
                    {
                        type: 'text',
                        value: ' !'
                    }
                ],
                emphasisType: 'bold'
            }
        ];

        expect(tokens).toEqual(expected);
    });
});

export {};
