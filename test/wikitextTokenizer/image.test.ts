import { type ImageToken } from "../../src/tokens/tokenTypes";
import { tokenize } from "../../src/tokens/wikitextTokenizer";

describe("Image links", () => {
    test("can start with 'File:'", () => {
        const tokens = tokenize("[[File:Example2.png|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: ImageToken = {
            type: 'image',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.png'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can start with 'Image:'", () => {
        const tokens = tokenize("[[Image:Example2.png|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: ImageToken = {
            type: 'image',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.png'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can appear midline", () => {
        const tokens = tokenize("Hello [[Image:Example2.png|150px |link=Main Page |alt=Alt text |Title text]] World");

        expect(tokens.map(x => x.type)).toEqual(["text", "image", "text"]);
    });
});

export {};
