import { NewlineToken } from "../../src/tokens/tokenTypes";
import { tokenize } from "../../src/tokens/wikitextTokenizer";

describe("Newlines", () => {
    test("can be represented by escape-n", () => {
        const tokens = tokenize("\n");

        const expected: NewlineToken = {
            type: 'newline'
        };
        expect(tokens).toEqual([expected]);
    });

    test("can follow each other", () => {
        const tokens = tokenize("\n\n\n");

        const expected: NewlineToken = {
            type: 'newline'
        };
        expect(tokens).toEqual([expected, expected, expected]);
    });

    test("can separate paragraphs", () => {
        const tokens = tokenize("Hello World\nEllo Werld");

        const expected: NewlineToken = {
            type: 'newline'
        };


        expect(tokens).toHaveLength(3);
        expect(tokens[1]).toEqual(expected);
    });
});

export {};
