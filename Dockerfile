FROM node:16.13.1-bullseye

WORKDIR /opt/wikipedia

ADD tsconfig.json ./
ADD package*.json ./
RUN npm install

ADD src/ ./src/

EXPOSE 3000
ENV NODE_ENV=production
CMD ["npm", "start"]
