import { Box } from '@mui/material';
import { SearchForm } from '../components/SearchForm';
import { useContext } from 'react';
import { ColorModeContext } from '@/components/colorMode';

export const FrontPage = () => {
    const { isDarkMode } = useContext(ColorModeContext);

    const boxBackgroundColor = isDarkMode
        ? 'rgba(32, 32, 32, .90)'
        : 'rgba(255, 255, 255, .90)';

    return (
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            height: '100%',
            background: "url('https://source.unsplash.com/random?orientation=landscape')",
            backgroundSize: 'cover'
        }}>
            <Box
                sx={{
                    boxShadow: 3,
                    width: '100%',
                    backgroundColor: boxBackgroundColor,
                    marginBottom: '128px',
                    paddingTop: '16px',
                    paddingBottom: '32px'
                }}>
                    <SearchForm />
            </Box>
        </Box>
    );
};

export default FrontPage;