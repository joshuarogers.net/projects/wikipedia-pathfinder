import SearchIcon from '@mui/icons-material/Search';
import { AppBar, Box, Button, Container, Divider, Fade, Grid, Link, Toolbar, Typography } from '@mui/material';
import { TimelineNavigator } from '@/components/TimelineNavigator';
import { useContext, useEffect, useState } from 'react';
import { findPath, getPage } from '@/libs/pathfinderServices';
import { Page } from "@/libs/Page";
import { PageSection } from '../components/PageSection';
import { useLoader } from '@/components/LoaderContext';
import { styled } from '@mui/material/styles';
import { usePathfinderNavigation } from '@/libs/navigation';
import { SearchForm } from '@/components/SearchForm';
import { PopoverElement } from '@/components/PopoverElement';
import { InternalLink } from '@/components/InternalLink';
import { CategoryWidget } from '@/components/CategoryWidget';
import { DarkModeToggle } from '@/components/DarkModeToggle';
import { ColorModeContext } from '@/components/colorMode';
import { Logo } from '@/components/Logo';
import { ScrollButton } from '@/components/ScrollButton';

const loadPageContents = async (setPage: (page: Page | null) => void, currentPageId: string | null) => {
    setPage(null);

    if (!currentPageId)
        return;

    const pageContent = await getPage(currentPageId);
    setPage(pageContent);
};

const loadPath = async (setPath: (path: (string | null)[]) => void, sourcePageId: string | null, destinationPageId: string | null) => {
    setPath([sourcePageId, null, destinationPageId]);

    if (sourcePageId === destinationPageId) {
        setPath([sourcePageId]);
        return;
    }

    const path = await findPath(sourcePageId, destinationPageId);
    if (path.length)
        setPath(path);
};

export const ResultsPage = () => {
    const loader = useLoader();
    const [page, setPage] = useState<Page | null>(null);
    const [path, setPath] = useState<(string | null)[]>([]);

    const { source, destination, pageId } = usePathfinderNavigation();
    const { isDarkMode } = useContext(ColorModeContext);

    const HeaderButton = isDarkMode
        ? styled(Button)({ 'color': '#FFF' })
        : styled(Button)({ 'color': 'inherit' });

    useEffect(() => {
        loader.withLoadScreen<void>(loadPageContents(setPage, pageId));
    }, [pageId]);

    useEffect(() => {
        loader.withLoadScreen<void>(loadPath(setPath, source, destination))
    }, [source, destination]);

    return (
        <>
            <AppBar position="fixed" color="primary" component="nav">
                <Toolbar>
                    {pageId && <HeaderButton href={`https://wikipedia.org/wiki/${encodeURI(pageId)}`}>View Original</HeaderButton>}
                    <Typography sx={{ flexGrow: 1 }}></Typography>
                    <HeaderButton href="/random">Random Path</HeaderButton>
                    <PopoverElement
                        triggerComponent={onClick => (
                            <HeaderButton onClick={x => onClick(x.currentTarget)} endIcon={<SearchIcon />}>Search</HeaderButton>
                        )}
                        popoverContent={onClose => (
                            <Container disableGutters sx={{
                                flexDirection: 'column',
                                padding: '10px',
                                width: '640px',
                                maxWidth: '100%'
                            }}>
                                <SearchForm
                                { ...{} /* @Todo: Fix form closing */ }
                                    initialSource={source}
                                    initialDestination={destination} />
                                <Divider sx={{ padding: '10px' }} />
                                <Typography variant='h6'>Quick Links</Typography>
                                <Box sx={{ display: 'flex', justifyContent: 'space-around'}}>
                                    <InternalLink path={{source: pageId}} onClick={onClose}>Start Here</InternalLink>
                                    <InternalLink path={{
                                        source: destination,
                                        destination: source
                                    }}
                                        onClick={onClose}>
                                            Reverse Direction
                                    </InternalLink>
                                    <InternalLink path={{destination: pageId}} onClick={onClose}>End Here</InternalLink>
                                </Box>
                            </Container>
                        )}
                    />
                </Toolbar>
            </AppBar>
            <Toolbar />
            <Container maxWidth="xl" disableGutters={false}>
                <Grid container spacing={2}>
                    <Grid item md={3} xs={12}>
                        <Link href="/">
                            <Box sx={{ height: "200px", display: "flex", alignItems: "center", justifyContent: "center" }}>
                                <Logo />
                            </Box>
                        </Link>
                        <Divider />
                        <TimelineNavigator links={path || []} />
                        <Divider />
                        {page?.categories && <CategoryWidget categories={page.categories} />}
                        <Divider />
                        <DarkModeToggle />
                    </Grid>
                    <Grid item md={9} xs={12}>
                        <Fade in={page !== null}>
                            <Box sx={{ top: "16px", position: "relative" }}>
                                {page !== null && (<>
                                    <Typography variant="h1">
                                        {page.title}
                                    </Typography>
                                    {page.alias !== page.title && page.alias && (<Typography variant="h5">
                                        Redirected from &quot;{page.alias}&quot;
                                    </Typography>)}
                                    {page.sections && page.sections.map((section, index) => (
                                        <PageSection section={section} key={index.toString()} />
                                    ))}
                                </>)}
                            </Box>
                        </Fade>
                    </Grid>
                </Grid>
            </Container>
            <ScrollButton />
        </>
    );
};

export default ResultsPage;
