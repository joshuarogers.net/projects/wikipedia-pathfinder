import { type Client as PostgresClient, type QueryResultRow} from "pg";
import _ from "lodash";
import { getPostgresClient } from "@/libs/data/postgres";

const getRandomPath = async (postgresConnection: PostgresClient) => {
    const querySingle = async <T extends QueryResultRow>(query: string, parameters?: unknown[]):Promise<T> =>
        (await postgresConnection.query<T>(query, parameters)).rows[0];
    
    const outgoingPageCount = await querySingle<{lastrow: number}>("SELECT MAX(id) AS lastrow FROM page_random_outgoing");
    const from = await querySingle<{title: string}>(
        `SELECT title
        FROM page
        INNER JOIN page_random_outgoing
        ON page.id = page_random_outgoing.page_id
        WHERE page_random_outgoing.id = $1
        ORDER BY page_random_outgoing.id ASC
        LIMIT 1`, [Math.floor(outgoingPageCount.lastrow * Math.random())]
    );

    const incomingPageCount = await querySingle<{lastrow: number}>("SELECT MAX(id) AS lastrow FROM page_random_incoming");
    const to = await querySingle<{title: string}>(
        `SELECT title
        FROM page
        INNER JOIN page_random_incoming
        ON page.id = page_random_incoming.page_id
        WHERE page_random_incoming.id = $1
        ORDER BY page_random_incoming.id ASC
        LIMIT 1`, [Math.floor(incomingPageCount.lastrow * Math.random())]
    );

    return {
        source: from['title'],
        destination: to['title']
    };
};

export async function getServerSideProps() {
    const postgresConnection = await getPostgresClient();
    const randomPath = await getRandomPath(postgresConnection);
    const redirectTarget = `/path?source=${encodeURIComponent(randomPath.source)}&destination=${encodeURIComponent(randomPath.destination)}`;

    return {
        redirect: {
            permanent: false,
            destination: redirectTarget,
        },
    };
}

export const RandomPage = () => (<></>);

export default RandomPage;
