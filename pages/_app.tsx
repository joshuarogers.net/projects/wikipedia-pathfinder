import type { NextPage } from 'next';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import '@/styles/globals.css';
import { ColorModeProvider } from '@/components/colorMode';
import { LoaderContextProvider } from '@/components/LoaderContext';
 
type AppPropsWithLayout = AppProps & {
    Component: NextPage;
    colorMode: 'light' | 'dark';
};

export default function App({ Component, pageProps, colorMode }: AppPropsWithLayout) {
    return (
        <>
            <Head>
                <title>Pathfinder</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
            </Head>
            <LoaderContextProvider>
                <ColorModeProvider colorMode={colorMode}>
                    <Component {...pageProps} />
                </ColorModeProvider>
            </LoaderContextProvider>
        </>
    );
}

App.getInitialProps = (context: any) => {
    return {
        colorMode: context?.ctx?.req?.cookies['colorMode'] || 'light'
    };
};
