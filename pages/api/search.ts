import _, { fromPairs } from "lodash";
import { getNeo4jClient } from "@/libs/data/neo4j";
import { getPostgresClient } from "@/libs/data/postgres";
import type { Driver, Path as neo4jPath } from "neo4j-driver";
import type { Client as PostgresClient } from "pg";
import type { NextApiRequest, NextApiResponse } from 'next';
import { normalizeAlias } from "@/utils";

const getPageId = async (postgresConnection: PostgresClient, alias: string): Promise<number | null> => {
    type PageIdentifier = { page_id: number };
    const pageIdentifier = await postgresConnection.query<PageIdentifier>(
        'SELECT page_id FROM page_alias WHERE normalized_title = $1', [normalizeAlias(alias)]);
    return pageIdentifier.rows[0]?.page_id;
}

const searchForPath = async (postgresConnection: PostgresClient, neo4jDriver: Driver, source: string, destination: string) => {
    const [sourceId, destinationId] = await Promise.all([getPageId(postgresConnection, source), getPageId(postgresConnection, destination)]);
    console.log(`${source}: ${sourceId}, ${destination}: ${destinationId}`)

    if (!sourceId || !destinationId || sourceId === destinationId)
        return [];

    const result = await neo4jDriver.session()
        .run({
            text: `MATCH (source:Page {id: $source})
                   MATCH (destination:Page {id: $destination})
                   MATCH path = shortestPath((source)-[:LINKS_TO*1..5]->(destination))
                   RETURN path`,
            parameters: { source: sourceId, destination: destinationId }});

    if (result.records.length === 0 || result.records[0].length === 0)
        return [];

    const record = result.records[0].get('path') as neo4jPath;
    const pageIds = _(record.segments)
        .map(x => [x.start.properties['id'], x.end.properties['id']])
        .flatten()
        .uniq()
        .value();

    const path = await postgresConnection.query<{id: number, title: string}>("SELECT id, title FROM page WHERE id = ANY($1)", [pageIds]);
    const titleByIdLookup = fromPairs(path.rows.map(x => [x.id, x.title]));
    return pageIds.map(x => titleByIdLookup[x]);
};

export default async function handler(request: NextApiRequest, response: NextApiResponse) {
    const postgresClient = await getPostgresClient();
    const neo4jClient = await getNeo4jClient();

    const pageTitles = await searchForPath(postgresClient, neo4jClient, request.query.source as string, request.query.destination as string);
    return response.json({
        results: pageTitles.map(x => ({ title: x }))
    });
}
