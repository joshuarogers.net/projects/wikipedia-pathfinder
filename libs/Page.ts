import { type HeadingToken, type Token } from "./tokens/tokenTypes";

export interface PageSection {
    heading: HeadingToken | null;
    content: Token[];
}

export interface Page {
    alias: string;
    title: string;
    sections: PageSection[];
    categories: string[];
}
