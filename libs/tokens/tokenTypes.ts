import type { StringReader } from "./stringReader";

export type Tokenizer = (stringReader: StringReader, tokenize: (input: string) => Token[]) => Token | Token[];

// All of the various token types that can appear in the array of results when
// calling tokenize. I've tried to capture all of the essentials that are represented
// via WikiText without focusing on making it a 1:1 tokenizer. Some things, such as images
// I don't expect to need for this project, so there isn't a reason for me to be able
// to represent them.
export interface HeadingToken {
    type: 'heading',
    value: Token[],
    level: number
}

export interface ImageToken {
    type: 'image',
    url: string
}

export interface InfoBoxValue {
    key: string,
    value: Token[]
}

export interface InfoBoxToken {
    type: 'infobox',
    infoBoxType: string,
    values: InfoBoxValue[]
}

export interface LinkToken {
    type: 'link',
    alias: string,
    page: string,
    relation: 'internal' | 'external'
}

export interface ListToken {
    type: 'list',
    listItems: Token[][]
    listType: 'ordered' | 'unordered'
}

export interface NewlineToken {
    type: 'newline'
}

export type TableCellAlignment = 'center' | 'right';

export interface TableCell {
    alignment?: TableCellAlignment, // @Todo: Certainly there are more ways to align
    width: number,
    height: number,
    values: Token[]
}

export type TableRow = TableCell[];

export interface TableToken {
    type: 'table',
    caption: Token[] | null,
    header: TableRow | null,
    rows: TableRow[]
}

export interface TextToken {
    type: 'text',
    value: string
}

export type TextEmphasisType = 'bold' | 'italic' | 'bold/italic';
export interface TextEmphasisToken {
    type: 'text-emphasis',
    tokens: Token[],
    emphasisType: TextEmphasisType
}

export interface UnsupportedToken {
    type: 'unsupported'
}

export interface VideoToken {
    type: 'video',
    url: string,
    format: 'webm' | 'mp4'
}

export type Token = HeadingToken | ImageToken | InfoBoxToken | LinkToken | ListToken | NewlineToken | TableToken | TextToken | TextEmphasisToken | UnsupportedToken | VideoToken;
