import type { Tokenizer } from "../tokenTypes";

export const tokenizeList: Tokenizer = (stringReader, tokenize) => {
    const listItemTexts = [];

    const delimiter = stringReader.peek(2);
    const listType = delimiter === '\n*'
        ? 'unordered'
        : 'ordered';

    do {
        stringReader.read(2);
        listItemTexts.push(stringReader.readUntil('stopBeforeMatch', '\n'));
    } while (stringReader.peek(2) === delimiter);

    return {
        type: 'list',
        listItems: listItemTexts.map(tokenize),
        listType
    };
}