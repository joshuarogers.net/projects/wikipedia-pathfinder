import { Tokenizer } from "../tokenTypes";

export const tokenizeHeading: Tokenizer = (stringReader, tokenize) => {
    const startToken = stringReader.readWhile(x => x === '=');
    const body = stringReader.readUntil('discardMatch', startToken);
    return {
        type: 'heading',
        value: tokenize(body),
        level: startToken.length
    };
};
