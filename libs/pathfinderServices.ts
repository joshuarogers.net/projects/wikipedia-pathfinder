import { first } from "lodash";
import { Page } from "./Page";
import { chunk } from "./reducer";
import type { LinkToken, HeadingToken, Token } from "./tokens/tokenTypes";
import { tokenize } from "./tokens/wikitextTokenizer";

interface SearchResult {
    title: string;
}

interface SearchResults {
    results: SearchResult[];
}

const pathfinderHost = "/api";

interface PageDto {
    alias: string,
    title: string,
    content: string
}

export const searchByAlias = async (criteria: string): Promise<string[]> => {
    if (!criteria)
        return [];

    const request = await fetch(`${pathfinderHost}/autocomplete?q=${encodeURIComponent(criteria.trim())}`);
    const response = await request.json() as SearchResults;
    return response.results.map(x => x.title);
};

export const findPath = async (source: string | null, destination: string | null): Promise<string[]> => {
    if (!source || !destination)
        return [];

    const request = await fetch(`${pathfinderHost}/search?source=${encodeURIComponent(source.trim())}&destination=${encodeURIComponent(destination.trim())}`);
    const response = await request.json() as SearchResults;
    return response.results.map(x => x.title);
};

export const getPage = async (alias: string): Promise<Page> => {
    const request = await fetch(`${pathfinderHost}/page/${encodeURIComponent(alias.trim())}`);
    const result = await request.json() as PageDto;

    const isTokenCategory = (token: Token) => token.type === 'link'
        && token.relation === 'internal'
        && token.page.startsWith('Category:');

    const tokens = tokenize(result.content);
    const contentTokens = tokens.filter(x => !isTokenCategory(x));

    const sections = chunk(contentTokens, token => token.type === 'heading')
        .map(content => {
            const hasHeader = first(content)?.type === 'heading';

            return {
                heading: hasHeader
                    ? first(content) as HeadingToken
                    : null,
                content: hasHeader
                    ? content.slice(1)
                    : content
            };
    });

    const categories = tokens
        .filter(isTokenCategory)
        .map(x => (x as LinkToken).page.replace('Category:', ''));

    return {
        title: result.title,
        alias: result.alias,
        sections,
        categories
    };
};
