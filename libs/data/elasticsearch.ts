import { memoize } from "lodash";
import { config } from "./config";
import { Client } from "@elastic/elasticsearch";

const createElasticSearchClient = async (): Promise<Client> => new Client({ node: config.elasticSearchUri });

export const getElasticSearchClient: () => Promise<Client> = memoize(createElasticSearchClient);
