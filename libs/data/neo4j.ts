import { memoize } from "lodash";
import { config } from "./config";
import { Driver, driver, auth } from "neo4j-driver";

const createNeo4jClient = async (): Promise<Driver> => {
    return driver(
        config.neo4JUri,
        auth.basic(config.neo4JUser, config.neo4JPass)
    );
};

export const getNeo4jClient: () => Promise<Driver> = memoize(createNeo4jClient);
