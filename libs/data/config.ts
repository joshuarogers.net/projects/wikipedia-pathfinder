import { env } from "process";

// Side-effecty import that updates process.env with contents of .env.
import "dotenv/config";

const getEnvOrThrow = (name: string): string => {
    const value = env[name];
    if (value)
        return value;

    throw Error(`Environment variable or .env setting "${name}" is not set`);
}

export const config = {
    /**
     * The HTTP port to be used by the Express web-server
     */
    expressPort: parseInt(getEnvOrThrow("EXPRESS_PORT")),

    /**
     * The URI for connecting to ElasticSearch
     */
    elasticSearchUri: getEnvOrThrow("ELASTICSEARCH_URI"),

    /**
     * The URI for connecting to Neo4J
     */
    neo4JUri: getEnvOrThrow("NEO4J_URI"),

    /**
     * The Username for Neo4J
     */
    neo4JUser: getEnvOrThrow("NEO4J_USER"),

    /**
     * The Password for Neo4J
     */
    neo4JPass: getEnvOrThrow("NEO4J_PASS"),

    /**
     * The Host for Postgres
     */
    postgresHost: getEnvOrThrow('POSTGRES_HOST'),

    /**
     * The Port for Postgres
     */
    postgresPort: parseInt(getEnvOrThrow('POSTGRES_PORT')),

    /**
     * The Username for Postgres
     */
    postgresUser: getEnvOrThrow('POSTGRES_USER'),

    /**
     * The Password for Postgres
     */
    postgresPass: getEnvOrThrow('POSTGRES_PASS'),

    /**
     * The Database name for Postgres
     */
    postgresDatabase: getEnvOrThrow('POSTGRES_DATABASE'),

    /**
     * A value representing a version of all endpoints such that it can be
     * used to identifying whether a cached response is still valid or should
     * be reloaded. The value itself has no meaning and is only used to see
     * "is this a value I've cached before."
     */
    cacheETag: getEnvOrThrow('CACHE_ETAG')
};
