import { memoize } from "lodash";
import { config } from "./config";
import { Client as PostgresClient } from "pg";

const createPostgresClient = async (): Promise<PostgresClient> => {
    const postgresClient = new PostgresClient({
        host: config.postgresHost,
        port: config.postgresPort,
        user: config.postgresUser,
        password: config.postgresPass,
        database: config.postgresDatabase
    });
    await postgresClient.connect();

    return postgresClient;
};

export const getPostgresClient: () => Promise<PostgresClient> = memoize(createPostgresClient);
