import { Parser } from "htmlparser2";
import { WritableStream } from "htmlparser2/lib/WritableStream";

export interface Namespace {
    id: number,
    name: string
}

export interface Page {
    id: number,
    namespaceId: number,
    title: string
}

export interface RedirectPage extends Page {
    target: string
}

export interface ContentPage extends Page {
    content: string
}

export type Handler<T> = (t: T) => Promise<void>;

export interface ParserCallback {
    onNamespace: Handler<Namespace>,
    onRedirect: Handler<RedirectPage>,
    onContent: Handler<ContentPage>
}

interface XMLTree {
    attributes: Record<string, string>,
    children: Record<string, XMLTree>,
    text: string[]
}

export const createWikipediaXMLStreamParser = ({onNamespace, onRedirect, onContent}: ParserCallback) => {
    const xmlTreeParseStack: XMLTree[] = [];

    let streamParser: Parser;
    let removeBackPressure: () => void;
    let isApplyingBackPressure = false;

    const dispatchNamespace = async (currentNode: XMLTree) => {
        const namespaces: Record<number, XMLTree> = currentNode.children.namespaces.children;
        for (const id in namespaces) {
            await onNamespace({
                id: parseInt(id, 10),
                name: namespaces[id].text.join('')
            });
        }
    };

    const dispatchPage = async (currentNode: XMLTree) => {
        const page: Page = {
            id: parseInt(currentNode.children.revision.children.id.text.join(''), 10),
            namespaceId: parseInt(currentNode.children.ns.text.join(''), 10),
            title: currentNode.children.title.text.join('')
        };

        const isContent = currentNode.children.redirect?.attributes.title === undefined;
        if (isContent) {
            await onContent({ ...page, content: currentNode.children.revision.children.text.text.join('') });
        } else {
            await onRedirect({ ...page, target: currentNode.children.redirect?.attributes.title });
        }
    };

    const withBackPressure = async (fn: () => Promise<void>) => {
        isApplyingBackPressure = true;
        streamParser.pause();

        await fn();

        // Why setImmediate? At the very end of the onclosetag callee, an internal index is incremented by one.
        // Calling pause and resume inside of the handler causes it to resume BEFORE incrementing the index,
        // leading to, unsurprisingly, corrupt data. Surrounding the resume logic with setImmediate pushes it
        // after the calling code finishes evaluating.
        setImmediate(() => {
            isApplyingBackPressure = false;
            streamParser.resume();

            // This seems like it shouldn't be a possible state, but it is: since the calling code does not know
            // about anything async, it continues running, not waiting for the promise of this method to resolve.
            // Because of that, in the case where the previous call is not yet finished, `isApplyingBackPressure`
            // will be true, since it is synchronously set to true at the start of the method and set to false
            // only after asychronous operations are complete. In the case where all processing is complete,
            // however, all async methods will have resolved, so that `isApplyingBackPressure` is false.
            //
            // This behaviour, obtuse as it is, gives us a second view into the system: in reviewing the
            // implementation of the parser, we can see that when the parser is running, it continues running
            // until one of two cases occur: the parser is paused manually or the buffer is exhausted, leaving
            // nothing else to parse. However, since the parser is only paused while an async block is running,
            // a case we know will cause `isApplyingBackPressure` to be true, we can understand that stopping
            // for an empty buffer, a case where no async task will be running, must give false. Thus, the value
            // of `isApplyingBackPressure``, when evaluated at this point gives an accurate indication of whether
            // `resume()` returned due to async work or due to buffer exhaustion, a case that means we should stop
            // apply backpressure.
            if (!isApplyingBackPressure)
                removeBackPressure();
        });
    };

    const writableStream = new WritableStream({
        onopentag: (_name, attributes) => {
            xmlTreeParseStack.push({
                attributes,
                children: {},
                text: []
            });
        },

        ontext: (text) => {
            xmlTreeParseStack.at(-1)!.text.push(text);
        },

        onclosetag: (name) => {
            const currentNode = xmlTreeParseStack.pop()!;

            if (name === 'siteinfo')
                withBackPressure(() => dispatchNamespace(currentNode));
            else if (name === 'page')
                withBackPressure(() => dispatchPage(currentNode));
            else if (xmlTreeParseStack.length > 0)
                xmlTreeParseStack.at(-1)!.children[currentNode.attributes["key"] ?? name] = currentNode;
        },
    }, { xmlMode: true });

    const previousWrite = writableStream._write;
    writableStream._write = (chunk: string | Buffer, encoding: string, callback: () => void): void => {
        previousWrite.bind(writableStream)(chunk, encoding, () => {
            if (isApplyingBackPressure)
                removeBackPressure = callback;
            else
                callback();
        });
    };

    streamParser = (writableStream as any)._parser;
    
    return writableStream;
};
