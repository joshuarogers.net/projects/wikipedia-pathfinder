import { Client } from "pg";
import { config } from "@/libs/data/config";
import { createWikipediaXMLStreamParser, type Namespace, type ContentPage, type RedirectPage, Handler } from "./parser";
import { tokenize } from "@/libs/tokens/wikitextTokenizer";
import { type Token } from "@/libs/tokens/tokenTypes";
import { normalizeAlias } from "@/utils";
import { uniq } from "lodash";


const getChildren = (token: Token): Token[] => {
    switch(token.type) {
        case 'heading':
            return token.value;
        case 'image':
            return [];
        case 'infobox':
            // This is not technically correct, but as I can't show all infoboxes, it creates "broken" paths.
            // Also, it doesn't really flow with the vibe of "random links in Wikipedia".
            return [];
        case 'link':
            return [];
        case 'list':
            return token.listItems.flatMap(x => x);
        case 'newline':
            return [];
        case 'table':
            return token.rows.flatMap(x => x).flatMap(x => x.values).concat(token.caption ?? []);
        case 'text':
            return []
        case 'text-emphasis':
            return token.tokens;
        case 'unsupported':
            return []
        case 'video':
            return [];
    }
};

function* flattenTokens(token: Token): Generator<Token> {
    yield token;

    const children = getChildren(token);
    for (const child of children)
        yield* flattenTokens(child);
}

interface Page {
    originalText: string;
    parsedPage: Token[];
    pageLinks: string[];
}

const parsePage = (text: string): Page => {
    const tokens = tokenize(text);
    const pageLinks = [];

    for (const tokenGenerator of tokens.map(flattenTokens)) {
        for (const token of tokenGenerator) {
            if (token.type === 'link' && token.relation === 'internal')
                pageLinks.push(token.page);
        }
    }

    return {
        originalText: text,
        parsedPage: tokens,
        pageLinks: pageLinks
    };
}

const run = async () => {
    const client = new Client({
        host: config.postgresHost,
        port: config.postgresPort,
        user: config.postgresUser,
        password: config.postgresPass,
        database: config.postgresDatabase
    });

    await client.connect();

    await client.query(`
        CREATE TABLE IF NOT EXISTS page(
            id INTEGER,
            title VARCHAR(512) NOT NULL,
            content TEXT COMPRESSION LZ4
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS page_current_alias_temp(
            id INTEGER,
            normalized_title VARCHAR(512) NOT NULL
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS page_previous_alias_temp(
            id INTEGER,
            title VARCHAR(512) NOT NULL,
            normalized_source_title VARCHAR(512) NOT NULL,
            normalized_target_title VARCHAR(512) NOT NULL
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS page_link_internal_temp(
            source_pageid INTEGER NOT NULL,
            normalized_target_title TEXT NOT NULL
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS category(
            id INTEGER,
            title VARCHAR(512) NOT NULL
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS category_page_temp(
            normalized_category_title TEXT NOT NULL,
            page_id INTEGER NOT NULL
        );
    `);

    await client.query(`
        CREATE TABLE IF NOT EXISTS category_hierarchy_temp(
            child_category_id INTEGER NOT NULL,
            parent_normalized_title VARCHAR(512) NOT NULL
        );
    `);

    await client.query(`BEGIN TRANSACTION`);
    
    const counter = {
        total: 0,
        pages: 0,
        pageAliases: 0,
        categories: 0,
        skipped: 0
    };

    console.time("Database Insertions");
    const tryCommit = async () => {
        if (counter.total % 1000 === 0) {
            await client.query("COMMIT TRANSACTION AND CHAIN");
            console.timeLog("Database Insertions");
            console.log(`Total: ${counter.total} / Pages: ${counter.pages} / Page Aliases: ${counter.pageAliases} / Categories: ${counter.categories} / NOOP: ${counter.skipped}`);
        }
    };

    const relevantNamespaceNames = new Set(["", "Category"]);
    const relevantNamespaceIds = new Set();

    const onNamespace = async (namespace: Namespace) => {
        if (relevantNamespaceNames.has(namespace.name))
            relevantNamespaceIds.add(namespace.id);
    };
    
    const onPage = async (contentPage: ContentPage) => {
        counter.pages++;

        const page = parsePage(contentPage.content);

        await client.query({
            name: 'add-page',
            text:`INSERT INTO page(id, title, content) VALUES($1, $2, $3)`,
            values: [contentPage.id, contentPage.title, contentPage.content]
        });

        await client.query({
            name: 'add-page-current-alias',
            text: 'INSERT INTO page_current_alias_temp(id, normalized_title) VALUES($1, $2)',
            values: [contentPage.id, normalizeAlias(contentPage.title)]
        })

        const targets = uniq(page.pageLinks)
            .filter(x => x.length <= 512)
            .map(normalizeAlias);

        const page_targets = targets.filter(x => !isCategory(x));
        if (page_targets.length > 0) {
            const page_values_clause = page_targets.map((_, i) => `($1, $${i + 2})`).join(', ');
            await client.query(
                `INSERT INTO page_link_internal_temp(source_pageid, normalized_target_title) VALUES ${page_values_clause}`,
                [contentPage.id, ...page_targets]
            );
        }

        const category_targets = targets.filter(isCategory);
        if (category_targets.length > 0) {
            const category_values_clause = category_targets.map((_, i) => `($1, $${i + 2})`).join(', ');
            await client.query(
                `INSERT INTO category_page_temp(page_id, normalized_category_title) VALUES ${category_values_clause}`,
                [contentPage.id, ...category_targets]
            );
        }
    };

    const onCategory = async (contentPage: ContentPage) => {
        counter.categories++;

        const page = parsePage(contentPage.content);

        await client.query({
            name: 'add-category',
            text:`INSERT INTO category(id, title) VALUES($1, $2)`,
            values: [contentPage.id, contentPage.title]
        });

        if (page.pageLinks.length > 0) {
            const targets = uniq(page.pageLinks)
                .filter(x => x.length <= 512)
                .map(normalizeAlias);
            const values = targets.map((_, i) => `($1, $${i + 2})`).join(', ');

            await client.query(
                `INSERT INTO category_hierarchy_temp(child_category_id, parent_normalized_title) VALUES ${values}`,
                [contentPage.id, ...targets]
            );
        }
    };

    const onPageRedirect = async (redirectPage: RedirectPage) => {
        counter.pageAliases++;

        await client.query({
            name: "add-page-previous-alias",
            text: `INSERT INTO page_previous_alias_temp(id, title, normalized_source_title, normalized_target_title) VALUES($1, $2, $3, $4)`,
            values: [redirectPage.id, redirectPage.title, normalizeAlias(redirectPage.title), normalizeAlias(redirectPage.target)]
        });
    };

    const onEnd = async () => {
        const sql = `
            ALTER TABLE category ADD PRIMARY KEY (id);
            CREATE UNIQUE INDEX ON category(title);
            CREATE INDEX ON category_hierarchy_temp(child_category_id);
            CREATE INDEX ON category_hierarchy_temp(parent_normalized_title);
            ALTER TABLE page ADD PRIMARY KEY (id);
            ALTER TABLE page_current_alias_temp ADD PRIMARY KEY(id);
            CREATE INDEX ON page_current_alias_temp(normalized_title);
            CREATE INDEX ON page_link_internal_temp(source_pageid);
            CREATE INDEX ON page_link_internal_temp(normalized_target_title);
            ALTER TABLE page_previous_alias_temp ADD PRIMARY KEY(id);
            CREATE INDEX ON page_previous_alias_temp(normalized_source_title);
            CREATE INDEX ON page_previous_alias_temp(normalized_target_title);

            VACUUM ANALYZE;

            -- Wikipedia exports aren't atomic, so if a page is renamed while the export is occurring,
            -- and then another page is renamed to the old name, there is a high likelihood that both
            -- the old and new page will appear with the same exact "unique" name. Since this breaks
            -- all kinds of logic, the best we can do is delete all but the newest copy (where newest
            -- is defined as the highest id.

            -- We use the page_current_alias_temp to decide which ones to keep since that page has
            -- all of the normalized varients of the current page titles.
            DELETE FROM page
            WHERE id IN (
                SELECT id FROM page_current_alias_temp
                EXCEPT SELECT MAX(id) FROM page_current_alias_temp
                GROUP BY normalized_title);

            DELETE FROM page_link_internal_temp
            WHERE source_pageid IN (
                SELECT id FROM page_current_alias_temp
                EXCEPT SELECT MAX(id) FROM page_current_alias_temp
                GROUP BY normalized_title);

            DELETE FROM page_current_alias_temp
            WHERE id IN (
                SELECT id FROM page_current_alias_temp
                EXCEPT SELECT MAX(id) FROM page_current_alias_temp
                GROUP BY normalized_title);

            DELETE FROM page_previous_alias_temp
            WHERE id IN (
                SELECT id FROM page_current_alias_temp
                EXCEPT SELECT MAX(id) FROM page_current_alias_temp
                GROUP BY normalized_title);

            DELETE FROM page_current_alias_temp
            WHERE id IN (
                SELECT id FROM page_current_alias_temp
                EXCEPT SELECT MAX(id) FROM page_current_alias_temp
                GROUP BY normalized_title);


            -- Another place that the lack of atomicity bites is when a single alias has been
            -- used to refer to two different pages while the export was occurring.
            DELETE FROM page_previous_alias_temp
            WHERE id IN (
                SELECT id FROM page_previous_alias_temp
                EXCEPT SELECT MAX(id) FROM page_previous_alias_temp
                GROUP BY normalized_source_title);


            -- A normalized title can't be both a page and an alias for another page
            -- but that doesn't stop the impossible from appearing in the export. If
            -- a page is renamed while the export from Wikipedia is running, then the
            -- original page AND the redirect both appear. For data consistency, we
            -- have to just choose one.
            DELETE from page_previous_alias_temp
            WHERE normalized_source_title IN (
                SELECT normalized_title FROM page_current_alias_temp);


            -- Flatten the tree of page redirects into a single "alias/id" lookup.
            CREATE TABLE page_alias AS
            WITH RECURSIVE alias(normalized_source_title, normalized_target_title) AS (
                    SELECT normalized_source_title, normalized_target_title
                    FROM page_previous_alias_temp
                UNION DISTINCT
                    SELECT alias.normalized_source_title, page_previous_alias_temp.normalized_target_title
                    FROM alias
                    INNER JOIN page_previous_alias_temp
                    ON alias.normalized_target_title = page_previous_alias_temp.normalized_source_title
            )
            SELECT alias.normalized_source_title as normalized_title, page_current_alias_temp.id as page_id
            FROM alias
            INNER JOIN page_current_alias_temp
            ON alias.normalized_target_title = page_current_alias_temp.normalized_title
            UNION
            SELECT normalized_title, id
            FROM page_current_alias_temp;

            ALTER TABLE page_alias ADD PRIMARY KEY (normalized_title);
            CREATE INDEX ON page_alias(page_id);
            ALTER TABLE page_alias ADD FOREIGN KEY (page_id) REFERENCES page(id);

            
            CREATE TABLE page_link_internal AS
                SELECT DISTINCT source_pageid, alias.page_id as target_pageid
                FROM page_link_internal_temp links
                INNER JOIN page_alias alias
                ON links.normalized_target_title = alias.normalized_title
                WHERE source_pageid != alias.page_id;
            
            CREATE INDEX ON page_link_internal(source_pageid);
            CREATE INDEX ON page_link_internal(target_pageid);
            ALTER TABLE page_link_internal ADD FOREIGN KEY (source_pageid) REFERENCES page(id);
            ALTER TABLE page_link_internal ADD FOREIGN KEY (target_pageid) REFERENCES page(id);
            
            CREATE TABLE category_hierarchy AS
                SELECT DISTINCT category.id as parent_category_id, category_hierarchy_temp.child_category_id
                FROM category_hierarchy_temp
                INNER JOIN category
                ON category_hierarchy_temp.parent_normalized_title = category.title;
            
            CREATE INDEX ON category_hierarchy(parent_category_id);
            CREATE INDEX ON category_hierarchy(child_category_id);
            ALTER TABLE category_hierarchy ADD FOREIGN KEY (parent_category_id) REFERENCES category(id);
            ALTER TABLE category_hierarchy ADD FOREIGN KEY (child_category_id) REFERENCES category(id);
            
            CREATE TABLE category_page AS
                SELECT DISTINCT category.id as category_id, category_page_temp.page_id
                FROM category_page_temp
                INNER JOIN category
                ON category_page_temp.normalized_category_title = category.title
                INNER JOIN page
                ON category_page_temp.page_id = page.id;
            
            CREATE INDEX ON category_page(category_id);
            CREATE INDEX ON category_page(page_id);
            ALTER TABLE category_page ADD FOREIGN KEY (category_id) REFERENCES category(id);
            ALTER TABLE category_page ADD FOREIGN KEY (page_id) REFERENCES page(id);
            

            CREATE TABLE page_random_outgoing(id serial PRIMARY KEY, page_id integer);
            INSERT INTO page_random_outgoing(page_id)
            SELECT source_pageid
            FROM page_link_internal
            GROUP BY source_pageid
            HAVING COUNT(source_pageid) >= 10;

            CREATE UNIQUE INDEX ON page_random_outgoing(page_id);
            ALTER TABLE page_random_outgoing ADD FOREIGN KEY(page_id) REFERENCES page(id);

            CREATE TABLE page_random_incoming(id serial PRIMARY KEY, page_id integer);
            INSERT INTO page_random_incoming(page_id)
            SELECT DISTINCT target_pageid
            FROM page_link_internal
            GROUP BY target_pageid
            HAVING COUNT(target_pageid) >= 10;

            CREATE UNIQUE INDEX ON page_random_incoming(page_id);
            ALTER TABLE page_random_incoming ADD FOREIGN KEY(page_id) REFERENCES page(id);

            DROP TABLE category_hierarchy_temp;
            DROP TABLE category_page_temp;
            DROP TABLE page_current_alias_temp;
            DROP TABLE page_previous_alias_temp;
            DROP TABLE page_link_internal_temp;
            
            VACUUM ANALYZE;        
        `;

        const commands = sql
            .split(";")
            .map(x => x.trim());

        console.time("sql");
        for (const command of commands) {
            const sanitizedCommand = command
                .split("\n")
                .map(x => x.trim())
                .filter(x => !x.startsWith("--"))
                .join("\n");

            console.log(sanitizedCommand);
            await client.query(sanitizedCommand);
            console.timeLog("sql");
        }
    };

    const isCategory = (title: string) => title.startsWith("Category:");
    const onContent: Handler<ContentPage> = async (page) => {
        counter.total++;

        if (!relevantNamespaceIds.has(page.namespaceId)) {
            counter.skipped++;
            return;
        }

        const handler = isCategory(page.title)
            ? onCategory
            : onPage;
        await handler(page);

        await tryCommit();
    };

    const onRedirect: Handler<RedirectPage> = async (page) => {
        counter.total++;

        if (!relevantNamespaceIds.has(page.namespaceId)) {
            counter.skipped++;
            return;
        }

        if (!isCategory(page.title))
            await onPageRedirect(page);

        await tryCommit();
    }
    
    
    const wikipediaXMLStreamParser = createWikipediaXMLStreamParser({ onNamespace, onContent, onRedirect });
    wikipediaXMLStreamParser.on("finish", async () => {
        client.query("COMMIT");
        await onEnd();
        process.exit(0);
    });

    process.stdin.pipe(wikipediaXMLStreamParser);
};

run();
