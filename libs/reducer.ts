import { last } from 'lodash';

export const chunk = <T>(items: T[], startNewChunkFn: (item: T) => boolean): T[][] => {
    const initialChunks: T[][] = [[]];
    return items.reduce((groupedItems, item) => {
        const isNewChunk = startNewChunkFn(item);
    if (isNewChunk)
    initialChunks.push([]);

    last(initialChunks)!.push(item);

    return groupedItems;
    }, initialChunks);
};
