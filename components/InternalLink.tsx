import { usePathfinderNavigation, type PathGenerationParameters } from "@/libs/navigation";
import { Link, LinkTypeMap } from "@mui/material";
import { DefaultComponentProps } from "@mui/material/OverridableComponent";

export interface InternalLinkProps extends Omit<DefaultComponentProps<LinkTypeMap>, "href"> {
    path: PathGenerationParameters;
}

export const InternalLink = (props: InternalLinkProps): JSX.Element => {
    const { generatePageUrl } = usePathfinderNavigation();
    return (<Link {...props} href={generatePageUrl(props.path)} />);
};
