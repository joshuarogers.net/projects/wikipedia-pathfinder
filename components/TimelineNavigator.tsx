import { Timeline, TimelineItem, TimelineSeparator, TimelineConnector, TimelineContent, TimelineDot } from '@mui/lab';
import { styled } from '@mui/material/styles';
import { useEffect, useState } from "react";
import { InternalLink } from './InternalLink';
import { usePathfinderNavigation } from '@/libs/navigation';

export interface TimelineNavigatorProps {
    links: (string | null)[];
}

export const TimelineNavigator = (props: TimelineNavigatorProps) => {
    const [selectedIndex, setSelectedIndex] = useState(0);
    const { pageId } = usePathfinderNavigation();

    useEffect(() => {
        setSelectedIndex(props.links.indexOf(pageId));
    }, [pageId, props.links]);

    const AnchoredTimelineItem = styled(TimelineItem)(({ theme }) => ({
        ':before': {
            display: 'none'
        }
    }));

    return (
        <Timeline position="left" sx={{ "li:last-child": { minHeight: 0 } }}>
            {props.links.map((pageId, i) => {
                return (<AnchoredTimelineItem key={i.toString()}>
                    <TimelineSeparator>
                        <TimelineDot variant={selectedIndex === i ? 'filled' : 'outlined'} />
                        {i + 1 !== props.links.length && (<TimelineConnector />)}
                    </TimelineSeparator>
                    <TimelineContent>
                        {pageId && <InternalLink path={{pageId}} underline="hover" color="inherit" onClick={() => setSelectedIndex(i)}>
                            {pageId}
                        </InternalLink> || "?"}
                    </TimelineContent>
                </AnchoredTimelineItem>)
            })}
        </Timeline>
    )
};