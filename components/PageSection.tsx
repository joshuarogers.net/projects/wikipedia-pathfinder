import { first } from 'lodash';
import { Typography } from "@mui/material";
import type { Token } from "@/libs/tokens/tokenTypes";
import type { TokenRenderer } from './renderers/rendererTypes';
import { renderHeading } from './renderers/renderHeading';
import { renderImage } from './renderers/renderImage';
import { renderLink } from './renderers/renderLink';
import { renderList } from './renderers/renderList';
import { renderTable } from './renderers/renderTable';
import { renderText, renderTextEmphasis } from './renderers/renderText';
import { renderUnsupported } from './renderers/renderUnsupported';
import { renderVideo } from './renderers/renderVideo';
import { type PageSection as PageSectionModel } from '@/libs/Page';
import { chunk } from '@/libs/reducer';

export interface PageSectionRendererProps {
    section: PageSectionModel
}

const tokenRenderers: Record<string, TokenRenderer<never>> = {
    'heading': renderHeading,
    'image': renderImage,
    'infobox': renderUnsupported,
    'link': renderLink,
    'list': renderList,
    'newline': renderUnsupported,
    'table': renderTable,
    'text': renderText,
    'text-emphasis': renderTextEmphasis,
    'unsupported': renderUnsupported,
    'video': renderVideo
};

export const PageSection = ({ section }: PageSectionRendererProps) => {
    const renderToken = (token: Token) => token.type in tokenRenderers
        ? tokenRenderers[token.type](token as never, renderToken)
        : null;

    const tokensByParagraph = chunk(section.content,
        token => token.type === 'newline' || token.type === 'list');

    return (<>
        {section.heading && renderToken(section.heading)}
        {tokensByParagraph.map(paragraph => {
            const isList = first(paragraph)?.type === 'list';
            if (isList)
                return paragraph.map(renderToken);

            // Only create line breaks for paragraphs containing rendered items
            const tokens = paragraph.map(renderToken).filter(x => x);
            return tokens.length > 0
                ? (<Typography sx={{ paddingBottom: '0.75em', paddingTop: '0.5em', paddingLeft: '2em', paddingRight: '2em' }}>{paragraph.map(renderToken)}</Typography>)
                : null;
        })}
    </>);
};
