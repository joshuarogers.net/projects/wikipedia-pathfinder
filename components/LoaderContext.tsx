import React, { createContext, ReactNode, useContext, useState } from 'react';
import { Backdrop, CircularProgress } from '@mui/material';

const LoaderContext = createContext<[number, React.Dispatch<React.SetStateAction<number>>] | null>(null);

export interface LoaderContextProviderProps {
    children: ReactNode;
}

export const LoaderContextProvider = (props: LoaderContextProviderProps) => {
    const [lockCount, setLockCount] = useState(0);

    return (<LoaderContext.Provider value={[lockCount, setLockCount]}>
        <LoadScreen />
        {props.children}
    </LoaderContext.Provider>);
};

export const useLoader = () => {
    const context = useContext(LoaderContext);

    if (context === null)
        throw new Error("useLoader can only be use inside of a <LoaderContextProvider>");

    const [_lockCount, setLockCount] = context;

    return {
        withLoadScreen: async <T,>(x: Promise<T>) => {
            try {
                setLockCount(x => x + 1);
                return await x;
            }
            finally {
                setLockCount(x => x - 1);
            }
        }
    };
};

export const LoadScreen = () => {
    const context = useContext(LoaderContext);

    if (context === null)
        throw new Error("<LoadScreen> can only be used inside of a <LoaderContextProvider>");

    const [lockCount, _setLockCount] = context;

    return (<Backdrop open={lockCount > 0} sx={{
        zIndex: theme => theme.zIndex.drawer + 1
    }}>
        <CircularProgress />
    </Backdrop>);
};
