import styled from "@emotion/styled";
import { Fab, Slide } from "@mui/material";
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';
import { useEffect, useState } from "react";

export const ScrollButton = () => {
    const [showScrollButton, setShowScrollButton] = useState(false);

    useEffect(() => {
        const checkScrollPosition = () => setShowScrollButton(window.scrollY > 500);

        window.addEventListener('scroll', checkScrollPosition);
        return () => window.removeEventListener('scroll', checkScrollPosition);
    }, []);

    const FixedLocationButton = styled(Fab)`position: fixed; bottom: 20px; right: 20px`;

    const scrollToTop = () => window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });

    return (
        <Slide direction="up" in={showScrollButton}>
            <FixedLocationButton variant="extended" onClick={scrollToTop}>
                <ArrowCircleUpIcon sx={{ mr: 1 }} /> Scroll to Top
            </FixedLocationButton>
        </Slide>
    );
};
