import { Button, Container, Grid } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import { SearchField } from "./SearchField";
import { useCallback, useState } from "react";
import { Logo } from "./Logo";
import { useRouter } from "next/router";

export interface SearchFormParams {
    initialSource?: string | null;
    initialDestination?: string | null;
}

export const SearchForm = (params: SearchFormParams) => {
    const {initialSource , initialDestination} = params;
    const [source, setSource] = useState<string | null>(initialSource || null);
    const [destination, setDestination] = useState<string | null>(initialDestination || null);
    const router = useRouter();

    const onSearch = useCallback((source: string | null, destination: string | null) => {
        if (!source || !destination)
            return;
        router.push(`/path?source=${encodeURIComponent(source)}&destination=${encodeURIComponent(destination)}`);
    }, [router]);

    return (<>
        <Container fixed maxWidth="md" sx={{
            flexDirection: 'column',
        }}>
            <Logo />
            <Grid container spacing={1}>
                <Grid item xs={12} sm={6}>
                    <SearchField
                        labelText="Source Page"
                        onSelect={setSource}
                        value={source} />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <SearchField
                        labelText="Destination Page"
                        onSelect={setDestination}
                        value={destination} />
                </Grid>
            </Grid>
            <Button variant="contained"
                endIcon={<SearchIcon />}
                size="large"
                sx={{ marginTop: '8px', width: '250px' }}
                disabled={!source || !destination}
                onClick={() => onSearch(source, destination)}>
                Search
            </Button>
        </Container>
    </>);
};
