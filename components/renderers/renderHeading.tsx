import { Typography } from "@mui/material";
import { type Variant } from "@mui/material/styles/createTypography";
import { clamp } from "lodash";
import { type HeadingToken } from "@/libs/tokens/tokenTypes";
import { type TokenRenderer } from './rendererTypes';

export const renderHeading: TokenRenderer<HeadingToken> = (token, renderToken) => {
    const level = clamp(token.level, 1, 6);
    const variant = `h${level}` as Variant;
    
    return (
        <Typography variant={variant}>{token.value.map(renderToken)}</Typography>
    );
};