import { Link } from "@mui/material";
import { type LinkToken } from "@/libs/tokens/tokenTypes";
import { InternalLink } from "../InternalLink";
import { type TokenRenderer } from './rendererTypes';

const renderInternalLink = (token: LinkToken) => (
    <InternalLink path={{ pageId: token.page }} color="secondary">
        {token.alias}
    </InternalLink>
);

const renderExternalLink = (token: LinkToken) => (
    <Link href={token.page}>
        {token.alias}
    </Link>
);

export const renderLink: TokenRenderer<LinkToken> = token => token.relation === 'internal'
    ? renderInternalLink(token)
    : renderExternalLink(token);
