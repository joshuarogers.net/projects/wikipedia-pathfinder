import { type TokenRenderer } from './rendererTypes';

export const renderUnsupported: TokenRenderer<never> = () => null;
