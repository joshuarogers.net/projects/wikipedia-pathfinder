import { type ListToken } from "@/libs/tokens/tokenTypes";
import { type TokenRenderer } from './rendererTypes';

export const renderList: TokenRenderer<ListToken> = (token, renderToken) => {
    const listItems = token.listItems.map((x, index) => (<li key={index.toString()}>{x.map(renderToken)}</li>));
    return token.listType === 'ordered'
        ? (<ol>{listItems}</ol>)
        : (<ul>{listItems}</ul>);
};
