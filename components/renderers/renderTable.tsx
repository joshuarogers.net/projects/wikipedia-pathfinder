import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { type TableToken } from "@/libs/tokens/tokenTypes";
import { type TokenRenderer } from './rendererTypes';

export const renderTable: TokenRenderer<TableToken> = (token, renderToken) => (
    <Paper sx={{ width: "100%", overflow: "scroll" }} elevation={2}>
        <TableContainer>
            <Table size="small">
                {token.caption !== null && (
                    <caption>{token.caption.map(renderToken)}</caption>
                )}
                {token.header !== null && (
                    <TableHead>
                        <TableRow>{token.header?.map((column, index) => (
                            <TableCell key={`column-${index}`}>
                                {column.values.map(renderToken)}
                            </TableCell>
                        ))}
                        </TableRow>
                    </TableHead>)}
                <TableBody>
                    {token.rows.map((row, rowIndex) => (
                        <TableRow key={`row-${rowIndex}`}>
                            {row.map((cell, columnIndex) => (
                                <TableCell rowSpan={cell.height} colSpan={cell.width} align={cell.alignment} key={`cell-${columnIndex}x${rowIndex}`}>
                                    {cell.values.map(renderToken)}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    </Paper>
);
